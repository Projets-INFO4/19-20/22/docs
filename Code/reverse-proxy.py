from flask import Flask, request, Response
import requests
from flask_restful import Resource, Api
from flask_caching import Cache
import os
import sys


app = Flask(__name__)
config = {
	"DEBUG": True,		# some Flask specific configs
	"CACHE_TYPE": "simple",	# Flask-Caching related configs
	"CACHE_DEFAULT_TIMEOUT": 300
}

app.config.from_mapping(config)
cache = Cache(app)
api = Api(app)

global NFSBaPa	# Absolute path to the directory that contains all the user directories (supposed to end with a '/')
global NFSInPa	# Optional path that goes from the user directory to the directory containing the files we want to read 
				# (also supposed to end with a '/')
global lastMeth	# Last method used to access the task

def DontUseCache():
	if(request):
		if(request.method=='GET'):
			if(lastMeth=='GET'):
				return False	# Use cache only in the case of multiple successive accesses to the same task using get
			else:
				return True
		else:
			return True
	else:
		return True

@app.route('/<string:login>/<string:task_ID>',methods=['GET','HEAD','POST','PUT','DELETE','CONNECT','OPTIONS','TRACE'])
@cache.cached(timeout=300,unless=DontUseCache())
# function called with all requests with a URL following the "server/login/task_ID" pattern
def proxy(login,task_ID):
	global NFSBaPa
	global NFSInPa
	global lastMeth
	lastMeth = request.method
	if(NFSInPa==None):
		complete_path = f'{NFSBaPa}{login}/{task_ID}'
	else:
		complete_path = f'{NFSBaPa}{login}/{NFSInPa}{task_ID}'
	if(not os.path.isfile(complete_path)):
		return {"ERROR":"Task not found"}
	urlfile = open(complete_path,"r")
	transfer_address = urlfile.read()
	transfer_address = transfer_address.rstrip("\n")
	transfer_address = transfer_address.rstrip(" ")
	if(not transfer_address.startswith('http')):
		transfer_address = f'http://{transfer_address}'
	if request.method=='GET':
		resp = requests.get(f'{transfer_address}')
		excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']	# headers that are not supposed to be transfered
		headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
		response = Response(resp.content, resp.status_code, headers)
		return response
	elif request.method=='HEAD':
		resp = requests.head(f'{transfer_address}')
		excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']	# headers that are not supposed to be transfered
		headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
		response = Response(resp.content, resp.status_code, headers)
		return response
	elif  request.method=='POST':
		resp = requests.post(f'{transfer_address}',json=request.get_json())
		excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']	# headers that are not supposed to be transfered
		headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
		response = Response(resp.content, resp.status_code, headers)
		return response
	elif  request.method=='PUT':
		resp = requests.put(f'{transfer_address}',json=request.get_json())
		excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']	# headers that are not supposed to be transfered
		headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
		response = Response(resp.content, resp.status_code, headers)
		return response
	elif request.method=='DELETE':
		resp = requests.delete(f'{transfer_address}')
		excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']	# headers that are not supposed to be transfered
		headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
		response = Response(resp.content, resp.status_code, headers)
		return response
	elif request.method=='CONNECT':
		resp = requests.connect(f'{transfer_address}')
		excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']	# headers that are not supposed to be transfered
		headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
		response = Response(resp.content, resp.status_code, headers)
		return response
	elif request.method=='OPTIONS':
		resp = requests.options(f'{transfer_address}')
		excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']	# headers that are not supposed to be transfered
		headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
		response = Response(resp.content, resp.status_code, headers)
		return response
	elif request.method=='TRACE':
		resp = requests.trace(f'{transfer_address}')
		excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']	# headers that are not supposed to be transfered
		headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
		response = Response(resp.content, resp.status_code, headers)
		return response
	else:
		return {"ERROR":"unknown method"}	# never supposed to be executed

if(__name__=='__main__'):
	global NFSBaPa
	global NFSInPa
	if(len(sys.argv)<2):
		print('missing base path')
		sys.exit(-1)
	else:
		NFSBaPa = sys.argv[1]	# getting the path to the directory that contains the user directories on the NFS
		if(len(sys.argv)==3):
			NFSInPa = sys.argv[2]	# getting the path going from the user directories to the directories containing the files to read
		else:
			NFSInPa = None
		app.run(debug = False,port=5000)
