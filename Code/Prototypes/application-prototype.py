from flask import Flask,request,redirect,Response, flash, render_template, g, session, abort
from flask_login import login_required, current_user, LoginManager

import requests
from flask_restful import Resource, Api
from flask_caching import Cache
import os


app = Flask(__name__)
SITE_NAME = 'https://www.youtube.com/'
config = {
    "DEBUG": True,          # some Flask specific configs
    "CACHE_TYPE": "simple", # Flask-Caching related configs
    "CACHE_DEFAULT_TIMEOUT": 300
}

app.config.from_mapping(config)
cache = Cache(app)
api = Api(app)

login_manager = LoginManager()
login_manager.init_app(app)	
login_manager.login_view = 'login'

lastMeth=None

@login_manager.user_loader	# tried to get the login information but all the methods we tried failed
def load_user(user_id):
    return User.get(user_id)


@app.route('/')	# also tried to use @app.need_authentication here to be able to get the login information in on()
def on():
	print(dir(current_user))
	name=load_user(login_manager.get_id())
	return {name:name}

def diffMeth():
    if lastMeth is None:
        return True
    else:
        if lastMeth==request.method:
            return False
        else:
            return True

@app.route('/<path:path>',methods=['GET','POST','DELETE'])
@cache.cached(timeout=10,unless=diffMeth())
def proxy(path):	# this function is called if the server receives a get, post or delete request, and gets the path after the first '/' of the URL as a parameter
	global SITE_NAME
	global lastMeth
	lastMeth = request.method
	if request.method=='GET':
		resp = requests.get(f'{SITE_NAME}{path}')
		excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
		headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
		response = Response(resp.content, resp.status_code, headers)
		return response

	elif  request.method=='POST':
		resp = requests.post(f'{SITE_NAME}{path}',json=request.get_json())
		excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
		headers = [(name, value) for (name, value) in resp.raw.headers.items() if name.lower() not in excluded_headers]
		response = Response(resp.content, resp.status_code, headers)
		return response
	elif request.method=='DELETE':
		resp = requests.delete(f'{SITE_NAME}{path}').content
		response = Response(resp.content, resp.status_code, headers)
		return response
	else:
		return {"result": "Task canceled"}

if __name__ == '__main__':
	app.run(debug = False,port=5000)
