from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

# simple to-do list test
# get server/ID or server/todo/ID to get a task
# put the same addresses to create or update a task

todos = {'1':'Coucou','2':'Oui','4':'TEST'}

class TodoSimple(Resource):
	def get(self, todo_id):
		return {todo_id: todos[todo_id]}

	def put(self, todo_id):
		todos[todo_id] = request.form['data']
		return {todo_id: todos[todo_id]}

api.add_resource(TodoSimple, '/<string:todo_id>', '/todo/<string:todo_id>')

if __name__ == '__main__':
	app.run(debug=True)
