from flask import Flask,request,redirect,Response
import requests

app = Flask(__name__)
#SITE_NAME is the path to the server where the request should be redirected
SITE_NAME = 'https://www.google.fr/'

@app.route('/')
def index():
	return 'Flask is running!'

@app.route('/<path:path>',methods=['GET','POST','DELETE'])
# path defines the url of the api dynamically.
# the function "proxy(path)" reads the request method and makes a request of the same type to SITE_NAME
def proxy(path):
	global SITE_NAME
	if request.method=='GET':
		resp = requests.get(f'{SITE_NAME}{path}')
		excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
		# choose the headers that can return from the server to the client through our proxy.
		# The headers that are in the lists are stopped by the proxy. Others can pass
		headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
		response = Response(resp.content, resp.status_code, headers)
		return response
	# We read & pass on a json in the method 'POST' to make sure the body of the request is passed.	
	elif request.method=='POST':
		resp = requests.post(f'{SITE_NAME}{path}',json=request.get_json())
		excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
		headers = [(name, value) for (name, value) in resp.raw.headers.items() if name.lower() not in excluded_headers]
		response = Response(resp.content, resp.status_code, headers)
		return response
	elif request.method=='DELETE':
		resp = requests.delete(f'{SITE_NAME}{path}').content
		response = Response(resp.content, resp.status_code, headers)
		return response

if __name__ == '__main__':
	#Debug set to False to avoid running every command twice.
	app.run(debug = False,port=5000)
