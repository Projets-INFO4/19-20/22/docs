from flask import Flask,request,redirect,Response
import requests
from flask_restful import Resource, Api
from flask_caching import Cache

app = Flask(__name__)
config = {
    "DEBUG": True,          # some Flask specific configs
    "CACHE_TYPE": "simple", # Flask-Caching related configs
    "CACHE_DEFAULT_TIMEOUT": 300
}
app.config.from_mapping(config)
cache = Cache(app)
api = Api(app)

todos = {'1':'TEST1','2':'TEST2','4':'TEST3'}
lastMeth=None

@app.route('/')
def index():
    return 'Server is up'

def diffMeth():
    if lastMeth is None:
        return True
    else:
        if lastMeth==request.method:
            return False
        else:
            return True

@app.route('/<string:todo_id>',methods=['GET','PUT','POST','DELETE'])
@cache.cached(timeout=20,unless=diffMeth())     # you can try the caching mechanism by deleting "unless=diffMeth()" 
                                                # and getting, then deleting and getting again the same task in less than 20 seconds
def proxy(todo_id):
    global todos
    global lastMeth
    lastMeth = request.method
    if request.method=='PUT' or request.method=='POST':
        todos[todo_id]=request.form['data']
    if request.method!='DELETE':
        return {todo_id: todos[todo_id]}
    else:
        todos[todo_id]=None
        return {"result": "Task canceled"}

if __name__ == '__main__':
	app.run(debug=True)

