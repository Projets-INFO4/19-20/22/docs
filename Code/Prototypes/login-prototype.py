from flask import Flask, flash, redirect, render_template, request, session, abort
import os

app = Flask(__name__)

@app.route('/')
@app.need_authentication()
def home():
    user = g.current_user
    return user


# https://github.com/oar-team/oar3/blob/master/oar/rest_api/views/job.py might help


if __name__ == "__main__":
    app.secret_key = os.urandom(24)
    app.run(debug=True,port=4000)


# did not work so we decided to use URLs following the pattern "server/login/task"
