# Log Book 

# Week 1 :  
- Discussion & exchange with Mr. Richard about the goal of the project.
- Familiarization with Flask / Python.
- Getting familiar with the subject

# Week 2 : 
 - Discussion around the architecture of the project
 - Discovery of Jupyter
 - Start of cache prototype for URLs
 - Start to write a simple reverse proxy application

#  Week 3 :
 - Familiarization with RESTful APIs

#  Week 4 :
 - RESTful API prototype

#  Week 5 :
 - Continuation of API prototyping
 - Reflection on the cache system of task / server ID associations
 - Preparation of the presentation

#  Week 6 :
 - Presentation
 - Cache and login prototypes

#  Week 7 :
 - Install OARdocker(We had some installation problems)
 - Dicovery of OAR and its tools

#  Week 8 :
 - Trying a simple reverse proxy application
 - Continuation of cache and login prototypes

#  Week 9-until now:
 - Working on final application
 - Working on report 
